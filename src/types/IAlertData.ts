import { AlertType } from "@/types/AlertType";

export interface IAlertData {
    message: string;
    dismissable?: boolean;
    type: AlertType;
}
