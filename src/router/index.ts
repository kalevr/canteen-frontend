import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/About.vue")
    },
    // Kitchen
    { path: "/kitchens", name: "Kitchens", component: () => import("../views/kitchens/Index.vue") },
    { path: "/kitchens/create", name: "KitchenCreate", component: () => import("../views/kitchens/Create.vue") },
    { path: "/kitchens/edit/:id?", name: "KitchenEdit", component: () =>
        import("../views/kitchens/Edit.vue"), props: true },
    { path: "/kitchens/details/:id?", name: "KitchenDetails", component: () =>
        import("../views/kitchens/Details.vue"), props: true },
    // KitchenView
    { path: "/kitchen-views", name: "KitchenViews", component: () => import("../views/kitchen-views/Index.vue") },
    { path: "/kitchen-views/create", name: "KitchenViewCreate", component: () => import("../views/kitchen-views/Create.vue") },
    { path: "/kitchen-views/edit/:id?", name: "KitchenViewEdit", component: () =>
        import("../views/kitchen-views/Edit.vue"), props: true },
    { path: "/kitchen-views/details/:id?", name: "KitchenViewDetails", component: () =>
        import("../views/kitchen-views/Details.vue"), props: true },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
