export interface IKitchen {
    id: string;
    kitchenName: string;
}

export interface IKitchenCreate {
    kitchenName: string;
}
