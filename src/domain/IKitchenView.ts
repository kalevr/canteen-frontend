export interface IKitchenView {
    id: string;
    kitchenViewName: string;
    kitchenViewDisplayName: string;
    startingFrom: string;
    validUntil?: string | undefined;
    mealTypeId: string;
}

export interface IKitchenViewCreate {
    kitchenViewName: string;
    kitchenViewDisplayName: string;
    startingFrom: string;
    validUntil?: string;
    mealTypeId: string;
}
