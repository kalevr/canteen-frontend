import { createApp, h, provide } from "vue";
import App from "./App.vue";
import { ApolloClient, ApolloLink, from, HttpLink, InMemoryCache } from "@apollo/client/core";
import { ApolloClients } from "@vue/apollo-composable";
import router from "@/router";
import "bootstrap/dist/css/bootstrap.css";


const token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5" +
  "L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjA4ZDk0Y2UyLWZjN2MtNDc1Zi04MjBhLTgxYmZjZjlkMWY5NSIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvY" +
  "XAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJhZG1pbkBsb2NhbGhvc3QiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy" +
  "93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJhZG1pbkBsb2NhbGhvc3QiLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHl" +
  "TdGFtcCI6Ik9BVENGTUpXVU5IRU00TlM0TE5DM0Q0NjQzU1k3RFhYIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lk" +
  "ZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2MzA0OTIzOTMsImlzcyI6ImVlLmVkdS5hbHV0YWd1c2UiLCJhdWQiOiJlZS5lZHUuY" +
  "Wx1dGFndXNlIn0.w5symTGNN24WN7GMhQaSVhrqsGLA25WK92NO8mB23O80PAdWMjIQqP9epzz3m3H5sGtQtSjQaqo-L5EBuidYeg";
// Made with the following tutorial
// https://www.newline.co/@kchan/building-a-graphql-application-with-vue-3-and-apollo--4679b402

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
        console.log('graphQLErrors', graphQLErrors);
    }
    if (networkError) {
        console.log('networkError', networkError);
    }
});

const httpLink = new HttpLink({ uri: "https://127.0.0.1:5001/graphql" });
const apolloLink = new ApolloLink((operation, forward) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    operation.setContext(({ headers }) => ({
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : null
        }
    }));
    return forward(operation); // Go to the next link in the chain. Similar to `next` in Express.js middleware.
});
const myLink = from([
    errorLink,
    apolloLink,
    httpLink
]);

const apolloClient = new ApolloClient({
    link: myLink,
    cache: new InMemoryCache()
});

const app = createApp({
    setup() {
        provide(ApolloClients, {
            default: apolloClient
        });
    },
    render: () => h(App)
}).use(router);

app.mount("#app");

//createApp(App).use(router).mount("#app");
import "bootstrap/dist/js/bootstrap";
import { onError } from "@apollo/client/link/error";
