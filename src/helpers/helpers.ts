import moment from "moment";

export default function formatDate(date: string) {
    const myDate = moment(date).format('DD.MM.YYYY');
    if (myDate == '31.12.9999') {
        return '';
    }
    return myDate;
}
