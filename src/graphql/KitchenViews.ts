import { gql } from "@apollo/client/core";

export const GET_KITCHEN_VIEWS = gql`
    query GetKitchenviews {
        kitchenViews {
            id
            kitchenViewName
            kitchenViewDisplayName
            startingFrom
            validUntil
            mealTypeId
            mealType {
                mealTypeName
            }
        }
    }
`;


export const GET_KITCHEN_VIEW_BY_ID_EDIT = gql`
    query GetKitchenviewByIdEdit($id: ID!) {
        kitchenViewById(id: $id) {
            id
            kitchenViewName
            kitchenViewDisplayName
            startingFrom
            validUntil
            mealTypeId
            mealType {
                id
                mealTypeName
            }
        },
        mealTypes {
            id
            mealTypeName
        }
    }
`;


export const GET_KITCHEN_VIEW_BY_ID = gql`
    query GetKitchenviewByIdEdit($id: ID!) {
        kitchenViewById(id: $id) {
            id
            kitchenViewName
            kitchenViewDisplayName
            startingFrom
            validUntil
            mealTypeId
            mealType {
                id
                mealTypeName
            }
        }
    }
`;

export const ADD_KITCHEN_VIEW = gql`
    mutation AddKitchenView($input: AddKitchenViewInput!) {
        addKitchenView(input: $input) {
            kitchenView {
                id
                kitchenViewName
                kitchenViewDisplayName
                startingFrom
                validUntil
                mealTypeId
            }
        }
    }
`;


export const UPDATE_KITCHEN_VIEW = gql`
    mutation UpdateKitchenView($input: UpdateKitchenViewInput!) {
        updateKitchenView(input: $input) {
            kitchenView {
                id
                kitchenViewName
                kitchenViewDisplayName
                startingFrom
                validUntil
                mealTypeId
            }
        }
    }
`;


export const DELETE_KITCHEN_VIEW = gql`
    mutation DeleteKitchenView($id: ID!) {
        deleteKitchenView(id: $id) {
            kitchenView {
                id
                kitchenViewName
                kitchenViewDisplayName
                startingFrom
                validUntil
                mealTypeId
                mealType {
                    mealTypeName
                }
            }
            errors {
                code
                message
            }
        }
    }
`;
