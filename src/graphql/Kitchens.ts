import { gql } from "@apollo/client/core";

export const GET_KITCHENS = gql`
    query GetKitchens {
        kitchens {
            id
            kitchenName
        }
    }
`;

export const GET_KITCHEN_BY_ID = gql`
    query GetKitchenById($id: ID!) {
        kitchenById(id: $id) {
            id
            kitchenName
        }
    }
`;

export const UPDATE_KITCHEN = gql `
    mutation UpdateKitchen($input: UpdateKitchenInput!) {
        updateKitchen(input: $input) {
            kitchen {
                id
                kitchenName
            }
        }
    }
`;

export const ADD_KITCHEN = gql `
    mutation AddKitchen($input: AddKitchenInput!) {
        addKitchen(input: $input) {
            kitchen {
                id
                kitchenName
            }
        }
    }
`;

export const DELETE_KITCHEN = gql `
    mutation DeleteKitchen($id: ID!) {
        deleteKitchen(id: $id) {
            kitchen {
                id
                kitchenName
            }
            errors {
                code
                message
            }
        }
    }
`;
