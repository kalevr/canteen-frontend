import { gql } from "@apollo/client/core";

export const GET_MEAL_TYPES = gql`
    query GetMealTypes {
        mealTypes {
            id
            mealTypeName
            mealTypeDisplayName
            mealTime
            registrationCutoff
            comment
            kitchen {
                id
                kitchenName
            }
        }
    }
`;
